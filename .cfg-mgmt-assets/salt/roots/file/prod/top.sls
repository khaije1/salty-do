##
## /srv/salt/roots/file/prod/top.sls - PROD environment specification
##


## note:
## - the job of the env/top.sls file is to match minions with environments
##   and environments with formulae, thereby matching minions w/ formulae.
## - the 'G@role:backup-operator' is a match applied to the minion used to gate the enclosed states.

