##
## /srv/salt/roots/file/base/top.sls - Base environment specification
##


## note: top file
## - this is a file of type: top 
## - - doc-srcs, http://docs.saltstack.com/ref/states/top.html
## - the job of the <env>/top.sls file is to match minions with environments
##   and environments with formulae, thereby matching minions w/ formulae.
## - top files in different environments are automatically combined 
##   when resolving minion-matches. The way this combination happens
##   worthy of note:
## - - nb, the "base" environment's top file is always processed first 
##     -no-matter-what- and is therefore -always- the authoritative
##     top file.
## - - if the base top file doesn't define the base environment, the
##     other environment's top are scanned, in -alphabetical-order- by
##     environment name until a section defining the "base" environment
##     is found. Once found, this first-found section is adopted as 
##     the "base" environment and any other/redundant/non-chosen base 
##     environment sections are invalidated and permenantly ignored.
## - - for any normal (non-"base") environments, the master looks for 
##     a section matching the environment name in the environment's
##     top file first, and if found - much as one would expect - it's used.
##     If the environment's top file _does_not_ include a section 
##     defining it's environment, then other top files are scanned, 
##     ordered by environment name, until a section defining
##     the environment is found. That section, where ever it is, is 
##     then used as the environment definition.
## - yaml semantics for this file type:
## - - 1st level, (ex. "base:" ), is the environment name for matching
## - - 2nd level, (ex. "  '*':" ), is the minion match criteria, this pattern
##     will filter/match minions according to a several different mechanisms,
##     detailed  below.
## - - - (default if unspecified) match type: "glob", as in shell glob
## - - - - ex. "  '*':", will match all minions (by id/name)
## - - - - ex. "  'webserver*' will match minions w/ id's that
##         start w/ "webserver"
## - - - match type: "pcre", as in perl-compatible-regular-expressions
## - - - - ex. "  '^(memcache|web).(qa|prod).loc$':"
## - - - match type: "grain"
## - - - - ex. "  'os:Ubuntu':"
## - - - match type: "grain_pcre"
## - - - - ex. "  'os:(RedHat|CentOS)':"
## - - - match type: "list"
## - - - - ex. "  'foo,bar,baz':"
## - - - match type: "pillar"
## - - - - ex. "  'somekey:someval':"
## - - - match type: "compound"
## - - - - ex. "  'nag1* of G@role:monitoring':"
## - - - match type "ipcidr", as in hosts by subnet
## - - - - ex. "  '10.10.201.0/24':"
## - - 3rd level, (ex. "    - dbg-stateconf-noop"), is the formulae name
##     to apply to any minion that matches the criteria in the 2nd level.
## - - - The master will search for a init.sls file (or directory) type
##       with this name among the roots defined as available to this 
##       environment (1st level), in the order listed, using the first 
##       match found without further search resolution.
base:
  '*':
    - dbg-stateconf-noop


